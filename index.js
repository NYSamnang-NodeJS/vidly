const express = require("express");
const path = require("path");

const app = express();

require("./startup/logging")();
require("./startup/routes")(app);
require("./startup/db")();
require("./startup/db")();
require("./startup/config")();
require("./startup/prod")(app);

app.set("view engine", "pug");
app.set("views", "./views");

const port = process.env.PORT || 3000;

const server = app.listen(port, () => console.log(`Listen on port ${port}...`));

module.exports = server;
