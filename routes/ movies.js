const express = require("express");

const { Movie, validateMovie } = require("../models/movie");
const { Genre } = require("../models/genre");
const { auth } = require("../middleware/auth");
const { admin } = require("../middleware/admin");
const { validateObjectId } = require("../middleware/validateObjectId");

const router = express.Router();

router.get("/", async (req, res) => {
  const movies = await Movie.find().sort("title");
  res.send(movies);
});

router.get("/:id", validateObjectId, async (req, res) => {
  const movie = await Movie.findById(req.params.id);
  if (!movie) {
    return res.status(404).send("The movie with given ID was not found.");
  }

  res.send(movie);
});

router.post("/", [auth], async (req, res) => {
  const { error } = validateMovie(req.body);
  if (error) {
    return res.status(400).send(error.details[0].message);
  }

  let genre;
  try {
    genre = await Genre.findById(req.body.genreId);
    if (!genre) {
      return res.status(400).send("The genre with given ID was not found.");
    }
  } catch (ex) {
    return res.status(404).send("The genre with given ID was not found.");
  }

  const movie = new Movie({
    title: req.body.title,
    genre: {
      _id: genre._id,
      name: genre.name
    },
    numberInStock: req.body.numberInStock,
    dailyRentalRate: req.body.dailyRentalRate
  });

  await movie.save();
  res.send(movie);
});

router.put("/:id", [auth, validateObjectId], async (req, res) => {
  const { error } = validateMovie(req.body);
  if (error) {
    return res.status(400).send(error.details[0].message);
  }

  let genre;
  try {
    genre = await Genre.findById(req.body.genreId);
    if (!genre) {
      return res.status(400).send("The genre with given ID was not found.");
    }
  } catch (ex) {
    return res.status(404).send("The genre with given ID was not found.");
  }

  const movie = await Movie.findByIdAndUpdate(
    req.params.id,
    {
      title: req.body.title,
      genre: {
        _id: genre._id,
        name: genre.name
      },
      numberInStock: req.body.numberInStock,
      dailyRentalRate: req.body.dailyRentalRate
    },
    { new: true }
  );

  if (!movie) {
    return res.status(404).send("The movie with given ID was not found.");
  }

  res.send(movie);
});

router.delete("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const movie = await Movie.findByIdAndRemove(req.params.id);
  if (!movie) {
    return res.status(404).send("The movie with given ID was not found.");
  }

  res.send(movie);
});

module.exports = router;
