const express = require("express");
const bcrypt = require("bcrypt");

const { User, validateUser } = require("../models/user");
const { auth } = require("../middleware/auth");
const { admin } = require("../middleware/admin");
const { validateObjectId } = require("../middleware/validateObjectId");

const router = express.Router();

router.get("/me", [auth], async (req, res) => {
  const user = await User.findById(req.user._id).select("-password");
  res.send(user);
});

router.get("/", [auth, admin], async (req, res) => {
  const user = await User.find()
    .select("-password")
    .sort("name");
  res.send(user);
});

router.get("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const user = await User.findById(req.params.id).select("-password");
  if (!user) {
    return res.status(404).send("The user with given ID was not found.");
  }
  const token = user.generateAuthToken();
  res.header("x-auth-token", token).send(user);
});

router.post("/", [auth, admin], async (req, res) => {
  const { error } = validateUser(req.body);
  if (error) {
    return res.status(400).send(error.details[0].message);
  }

  let user = await User.findOne({ email: req.body.email });
  if (user) {
    return res.status(400).send("Email already exist.");
  }

  user = new User({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    isAdmin: req.body.isAdmin
  });

  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);

  await user.save();

  const token = user.generateAuthToken();
  res.header("x-auth-token", token).send({
    _id: user._id,
    name: user.name,
    email: user.email,
    isAdmin: user.isAdmin
  });
});

router.put("/:id", [auth, admin, validateObjectId], async (req, res) => {
  if (
    req.params.id === "5b35b178993b6c0395ca8d4d" ||
    req.params.id === "5b3ef9df539918001409f21d" ||
    req.params.id === "5b460deb9d03610014a90f19"
  ) {
    return res.status(403).send("You cannot modify my account :D");
  }

  const { error } = validateUser(req.body);
  if (error) {
    return res.status(400).send(error.details[0].message);
  }

  const user = await User.findById(req.params.id);
  if (!user) {
    return res.status(404).send("The user with given ID was not found.");
  }

  user.set({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    isAdmin: req.body.isAdmin
  });

  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);

  await user.save();

  const token = user.generateAuthToken();
  res.header("x-auth-token", token).send({
    _id: user._id,
    name: user.name,
    email: user.email,
    isAdmin: user.isAdmin
  });
});

router.delete("/:id", [auth, admin, validateObjectId], async (req, res) => {
  if (
    req.params.id === "5b35b178993b6c0395ca8d4d" ||
    req.params.id === "5b3ef9df539918001409f21d" ||
    req.params.id === "5b460deb9d03610014a90f19"
  ) {
    return res.status(403).send("You cannot modify my account :D");
  }

  const user = await User.findByIdAndRemove(req.params.id);
  if (!user) {
    return res.status(404).send("The user with given ID was not found.");
  }

  res.send(user);
});

module.exports = router;
