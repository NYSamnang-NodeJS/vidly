const express = require("express");
const mongoose = require("mongoose");
const Fawn = require("fawn");

const { Rental } = require("../models/rental");
const { Customer } = require("../models/customer");
const { Movie } = require("../models/movie");
const { auth } = require("../middleware/auth");
const { admin } = require("../middleware/admin");
const { validateObjectId } = require("../middleware/validateObjectId");

const router = express.Router();

Fawn.init(mongoose);

router.get("/", [auth], async (req, res) => {
  const rentals = await Rental.find();
  res.send(rentals);
});

router.get("/:id", [auth, validateObjectId], async (req, res) => {
  const rental = await Rental.findById(req.params.id);
  if (!rental) {
    return res.status(404).send("The rental with given ID was not found.");
  }

  res.send(rental);
});

router.post("/", [auth], async (req, res) => {
  let customer;
  try {
    customer = await Customer.findById(req.body.customerId);
    if (!customer) {
      return res.status(404).send("The customer with given ID was not found.");
    }
  } catch (ex) {
    return res.status(404).send("The customer with given ID was not found.");
  }

  let movie;
  try {
    movie = await Movie.findById(req.body.movieId);
    if (!movie) {
      return res.status(404).send("The movie with given ID was not found.");
    }
    if (movie.numberInStock === 0) {
      return res.status(404).send("The movie with given ID is not in stock.");
    }
  } catch (ex) {
    return res.status(404).send("The movie with given ID was not found.");
  }

  const rental = new Rental({
    customer: {
      _id: customer._id,
      name: customer.name,
      phone: customer.phone,
      isGold: customer.isGold
    },
    movie: {
      _id: movie._id,
      title: movie.title,
      dailyRentalRate: movie.dailyRentalRate
    }
  });

  try {
    new Fawn.Task()
      .save("rentals", rental)
      .update("movies", { _id: movie._id }, { $inc: { numberInStock: -1 } })
      .run();

    res.send(rental);
  } catch (ex) {
    return res.status(500).send("Something went wrong...");
  }
});

router.delete("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const rental = await Rental.findByIdAndRemove(req.params.id);
  if (!rental) {
    return res.status(404).send("The rental with given ID was not found.");
  }

  movie = await Movie.findById(rental.movie._id);
  movie.numberInStock++;
  movie.save();

  res.send(rental);
});

module.exports = router;
