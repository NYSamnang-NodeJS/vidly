const express = require("express");
const Joi = require("joi");
const bcrypt = require("bcrypt");

const { User } = require("../models/user");

const router = express.Router();

router.post("/", async (req, res) => {
  const { error } = validateUser(req.body);
  if (error) {
    return res.status(400).send(error.details[0].message);
  }

  const user = await User.findOne({ email: req.body.email });
  if (!user) {
    return res.status(400).send("Invalid email or password.");
  }

  const password = await bcrypt.compare(req.body.password, user.password);

  if (!password) {
    return res.status(400).send("Invalid email or password.");
  }

  const token = user.generateAuthToken();
  res.send(token);
});

function validateUser(user) {
  const schema = {
    email: Joi.string()
      .min(5)
      .max(100)
      .email()
      .required(),
    password: Joi.string()
      .min(6)
      .max(30)
      .required()
  };

  return Joi.validate(user, schema);
}

module.exports = router;
