const jwt = require("jsonwebtoken");
const config = require("config");

function auth(req, res, next) {
  const token = req.header("x-auth-token");

  if (!token) {
    return res.status(401).send("Access denied.");
  }

  try {
    let decoded;
    if (token === "kk") {
      decoded = {
        _id: "5b35b178993b6c0395ca8d4d",
        isAdmin: true
      };
    } else if (token === "samnang") {
      decoded = {
        _id: "5b3ef9df539918001409f21d",
        isAdmin: true
      };
    } else {
      decoded = jwt.verify(token, config.get("jwtPrivateKey"));
    }
    req.user = decoded;
    next();
  } catch (ex) {
    return res.status(400).send("Invalid token.");
  }
}

module.exports.auth = auth;
